#!/bin/bash
######################################
# Functions for de novo assembly and assembly quality check
# Author: Si-Nguyen Mai (nguyenmts@oucru.org)
# Date: 2023
######################################

assem_unicycler(){
# Assembly with Unicycler for one sample
    local data_dir="$1"; # directory containing raw reads files with tails _{1,2}.fastq.gz
    local sampleID="$2";
    local outdir="$3"; # output directory for a sample

    # number of threads/cores for parallel processing
    if [ -z "$4" ]; then
        local nt=8;
    else 
        local nt=$4;
    fi;

    echo "Performing assembly with Unicycler for sample ..." $sampleID;
    unicycler -1 "$data_dir"/"$sampleID"_1.fastq.gz \
        -2 "$data_dir"/"$sampleID"_2.fastq.gz \
        -o "$outdir" --threads $nt;
}
export -f assem_unicycler

assem_unicycler_series(){
# Assembly with Unicycler for multiple samples in multiple cores
    local indir="$1"; # directory containing raw reads files with tails _{1,2}.fastq.gz
    local parent_outdir="$2"; # directory containing output directories for every sample
    mkdir -p $parent_outdir;

    # number of threads/cores for parallel processing
    if [ -z "$3" ]; then
        local nt=10;
    else 
        local nt=$3;
    fi;

    conda activate tormes-1.3.0;
    for file in "$indir"/*_1.fastq.gz; do
        local filename=${file##*/};
        local sampleID=${filename%*_1.fastq.gz};

        local outdir="${parent_outdir}/${sampleID}"; # output directory for a sample

        assem_unicycler "$indir" "$sampleID" "$outdir" "$nt";
    done;
}

assembly_qc(){
# Assessing contamination and assembly quality with CheckM
    local indir="$1"; # input dir containing assembly .fasta files
    local outdir="$2"; # output dir
    mkdir -p $outdir;

    local genus="$3"; # expected genus
    
    # number of threads/cores for parallel processing
    if [ -z "$4" ]; then
        local nt=10;
    else 
        local nt=$4;
    fi;

    conda activate checkm-genome;

    checkm taxonomy_wf -t $nt -x fasta \
        genus $genus $indir $outdir >\
            ${outdir}"/checkm_taxonomy.runlog" 2>&1;

    # Get the statistics - Completeness, Contamination, Strain Heterogeneity
    # As well as Genome (assembly) Size, # contigs, N50
    checkm qa -o 2 ${outdir}"/"${genus}".ms" \
        ${outdir} >\
        ${outdir}"/checkm_taxonomy.stats";

}

trimming () {
# Trimming reads with Trimmomatic
# Remove adapters NexteraPE-PE.fa
# Head crop the first 15 bp. 
# Sliding windows with length of [?] bp, quality threshold [?]. 
# Min read length of [?] bp.
    local indir="$1";
    local sample="$2";
    
    local outdir="$3";

    local adapters="/home/ubuntu/mambaforge/envs/tormes-1.3.0/share/trimmomatic-0.39-2/adapters/NexteraPE-PE.fa";

    conda activate tormes-1.3.0
    trimmomatic PE ${indir}/${sample}_1.fastq.gz ${indir}/${sample}_2.fastq.gz \
        "$outdir"/"$sample"_1_paired.fastq.gz  "$outdir"/"$sample"_1_unpaired.fastq.gz \
        "$outdir"/"$sample"_2_paired.fastq.gz  "$outdir"/"$sample"_2_unpaired.fastq.gz \
        ILLUMINACLIP:"$adapters":2:30:10 \
        LEADING:3 TRAILING:3 SLIDINGWINDOW:$4:$5 MINLEN:$6 \
        HEADCROP:15 \
        2>&1 | tail -n 2 | \
        awk -v id="$sample" '{OFS="\t"} {print id,$0}' >> "$outdir"/trimming_summary
}
