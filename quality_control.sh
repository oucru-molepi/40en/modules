#!/bin/bash
######################################
# Functions for quality check on the sequencing data
# Author: Si-Nguyen Mai (nguyenmts@oucru.org)
# Date: 2023
######################################

wgs_qc(){
# Runs FASTQC and Multi-QC
    local indir=$1;
    local fastqc_outdir=$2;
    local multiqc_outdir=$3;
    
    # number of threads/cores for parallel processing
    if [ -z "$4" ]; then
        local nt=10;
    else 
        local nt=$4;
    fi;

    mkdir -p $fastqc_outdir;
    conda activate multiqc;
    fastqc -o $fastqc_outdir -t $nt $indir/*.fastq.gz;
    multiqc --interactive --force -o $multiqc_outdir $fastqc_outdir;
}

# taxanomy_metaphlan2_sp(){
# # Taxanomy profiling with MetaPhlAn2, at species level
# # uses environment variable: `mpa_dir`
#     local data_dir=$1;
#     local output_dir=$2;
#     mkdir -p $output_dir;
#     # number of threads/cores for parallel processing
#     if [ -z "$3" ]; then
#         local nt=10;
#     else 
#         local nt=$3;
#     fi;

#     conda activate py38;
#     for file in "$data_dir"/*_1.fastq.gz; do
#         filename=${file##*/};
#         sampleID=${filename%*_1.fastq.gz};

#         input="${data_dir}/${sampleID}_1.fastq.gz,${data_dir}/${sampleID}_2.fastq.gz";

#         metaphlan2.py "$input" --input_type fastq \
#             --tax_lev 's' \
#             --bowtie2out "$sampleID".bowtie2.bz2 --no_map \
#             --sample_id "$sampleID" \
#             --nproc "$nt"  > "$output_dir"/"$sampleID".metaphlan.txt;
#     done;

#     python ${mpa_dir}/utils/merge_metaphlan_tables.py ${output_dir}/*.metaphlan.txt > ${output_dir}/merged_abundance_table.tsv;
# }

taxanomy_metaphlan4_sp(){
# Taxanomy profiling with MetaPhlAn4, at species level
    local data_dir=$1;
    local output_dir=$2;
    mkdir -p $output_dir;
    # number of threads/cores for parallel processing
    if [ -z "$3" ]; then
        local nt=10;
    else 
        local nt=$3;
    fi;

    conda activate metaphlan4;
    for file in "$data_dir"/*_1.fastq.gz; do
        local filename=${file##*/};
        local sampleID=${filename%*_1.fastq.gz};

        local input="${data_dir}/${sampleID}_1.fastq.gz,${data_dir}/${sampleID}_2.fastq.gz";

        metaphlan "$input" --input_type fastq \
            --tax_lev 's' \
            --bowtie2out "$sampleID".bowtie2.bz2 --no_map \
            --sample_id "$sampleID" \
            --nproc "$nt"  > "$output_dir"/"$sampleID".metaphlan.txt;
    done;

    merge_metaphlan_tables.py ${output_dir}/*.metaphlan.txt > ${output_dir}/merged_abundance_table.tsv;
}