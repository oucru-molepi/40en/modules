#!/bin/bash
listFile=$1 # .tsv file with sample (run) accessions
# the file should have 1st column with study accessions
# and the 2nd column with sample (run) accessions
prefix=$2

p="None"
while IFS=$'\t' read -r study_acc run_acc; do
    
    if [ "$study_acc" != "$p" ]; then
        p="$study_acc";
        outdir="${prefix}_${p}";
        mkdir -p $outdir;
    fi;

    enaDataGet -f fastq -d $outdir -m $run_acc;

done < <(tail -n +2 $listFile | cut -f1,2)