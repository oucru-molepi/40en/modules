#!/usr/bin/env python

######################################
# Processing abundance table from MetaPhlAn4 to check for purity of a specified species
# Author: Si-Nguyen Mai (nguyenmts@oucru.org)
# Date: 2023
# Python 3
######################################

import pandas as pd
import argparse

def assess(metaphlan_file: str, target_species: str, threshold: float):
    df = pd.read_csv(metaphlan_file, sep="\t", skiprows=[0])
    df.columns = [x.replace(".metaphlan", "") for x in df.columns]

    target_df = df.set_index('clade_name').loc["s__" + target_species, :]

    # total number of samples
    total_N = len(target_df)
    print(f"Number of samples with MetaPhlAn data checked: {total_N}.")

    # samples that had < {threshold} percentage of {target_species}
    unsatisfied_samples = target_df.index[target_df < threshold].to_list()

    # Check if there is no sample had < {threshold} percentage of {target_species}
    if len(unsatisfied_samples) == 0:
        print(f"All samples had >= {threshold} percentage of {target_species}.")
    else:
        # Return the metaphlan table of "unsatisfied" samples
        unsatisfied_df = df.set_index('clade_name').loc[:, unsatisfied_samples]
        print(f"Number of samples had < {threshold} percentage of {target_species}: {len(unsatisfied_samples)}.")
        print(unsatisfied_df.to_markdown())
    
    return(unsatisfied_samples)

if __name__ == '__main__':
    
    # default_file = "../raw_read/B611-94/metaphlan4_040823/merged_abundance_table.tsv"
    default_species = "Klebsiella_pneumoniae"
    default_threshold = 90.0

    parser = argparse.ArgumentParser(description="Analyze the merged_abundance_table.tsv output from MetaPhlAn4. Check if all samples mainly contain a single targeted species.")
    parser.add_argument("-f", "--metaphlan_file", 
                        help="The merged_abundance_table.tsv file from MetaPhlAn4", 
                        type=argparse.FileType('r'), required=True)
    parser.add_argument("-s", "--target_species", 
                        help="Name of the targeted species. Words should be separated by \"_\". (default: {})".format(default_species), 
                        type=str, default=default_species)
    parser.add_argument("-t", "--threshold", 
                        help="The percentage threshold for the abundance of the targeted species. (default: {})".format(default_threshold), 
                        type=float, default=default_threshold)
    parser.add_argument("-o", "--unsatisfied_list_file", 
                        help="Name for a file containing the list of samples with percentage of the targeted species < threshold. Optional, if not specified, no file will be output.",
                        type=str, default=None)
    args = parser.parse_args()

    output = assess(args.metaphlan_file, args.target_species, args.threshold)

    if (len(output) > 0) and (args.unsatisfied_list_file is not None):
        with open(args.unsatisfied_list_file, 'w') as f:
            for s in output:
                f.write(f"{s}\n")
