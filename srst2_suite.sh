#!/bin/bash
######################################
# Functions for MLST and plasmid typing with SRST2 v0.2.0
# Author: Si-Nguyen Mai (nguyenmts@oucru.org)
# Date: 2023
######################################

srst2_mlst(){
# MLST typing with the default settings:
#   `--mlst_max_mismatch 10` - Maximum number of mismatches per read for MLST allele calling
#   `--min_depth 5` - Minimum mean depth to flag as dubious allele call
#   `--min_edge_depth 2` - Minimum edge depth to flag as dubious allele call

    local indir="$1"; # directory containing raw reads files with tails _{1,2}.fastq.gz
    local outdir="$2"; # output directory
    mkdir -p $outdir;

    local species="$3"; # species name, words separated by "_"
    local sp=${species/"_"/" "};

    # number of threads/cores for parallel processing
    if [ -z "$4" ]; then
        local nt=10;
    else 
        local nt=$4;
    fi;

    (
        cd $outdir;
        conda activate srst2;

        # Create MLST database
        echo "Creating MLST database for species: ${sp} ..."
        mkdir -p db;
        cd db;
        getmlst.py --species "${sp}";
        cd ..;

        # MLST typing
        echo "MLST typing with SRST2 v0.2.0 on sequencing reads in directory:";
        echo -e "............ ${indir} ...";
        local label=${indir##*/};
        srst2 --input_pe ${indir}/*.fastq.gz \
            --output $label \
            --log --save_scores \
            --mlst_db db/${species}.fasta --mlst_definitions db/profiles_csv \
            --mlst_delimiter '_' \
            --threads $nt;
    )
}

# srst2_plasmid(){
# # Plasmid typing
# }