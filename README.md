# Computing modules for 40EN project

Scripts containing functions for processing and analyzing WGS data of the 40EN project.

These were only tested and used in a single computing environment, which is a HPC cluster at OUCRU Ho Chi Minh.

## Environment
Ubuntu 18.04.2 LTS <br>
Architecture:        x86_64 <br>
CPU op-mode(s):      32-bit, 64-bit <br>
Byte Order:          Little Endian <br>
CPU(s):              10 <br>
Vendor ID:           GenuineIntel <br>
CPU family:          6 <br>
Model:               85 <br>
Model name:          Intel Xeon Processor (Cascadelake) <br>
Stepping:            6 <br>
CPU MHz:             2095.078 <br>
BogoMIPS:            4190.15

## Dependencies
- FASTQC v.0.11.8 (conda env multiqc)
- MultiQC v1.14 (conda env multiqc)
- CheckM v1.2.2 (conda env checkm-genome)
- Trimmomatic v0.39 (conda env tormes-1.3.0)
- Unicycler v0.5.0, in company with SPAdes v3.15.2 (conda env tormes-1.3.0)
- QUAST v5.0.2 (conda env tormes-1.3.0)
- MetaPhlAn v4.0.6 (conda env metaphlan4)

- Kleborate v2.3.2 + Kaptive v2.0.6 (conda env py38)
- SRST2 v0.2.0 (conda env srst2)
- ABRicate v1.0.1 (conda env tormes-1.3.0)
- abriTAMR v1.0.14 (conda env abritamr)
    - comes with AMRFinder v3.10.42 and AMRFinder Plus database version 2022-08-09.1

- Bakta
    - v1.6.1, with database at `/data/SiNguyen/bakta_db_4.0_2022-08-29/` (conda env bakta-1.6.1)
    - v1.8.1, with database at `/data/SiNguyen/bakta_db_5.0_2023-02-20/` (conda env bakta)

- nf-core/bactmap v1.0.0 (conda env nextflow)
- Snippy v4.6.0
- Gubbins v3.2.1 (conda env gubbins), comes with
    - raxmlHPC v8.2.12
    - FastTree v2.1.10
    - IQ-TREE v2.0.3

- remove_blocks_from_aln.py (conda env srst2, python=2.7)
- SNP-sites v2.3.3

- FigTree v1.4.4
